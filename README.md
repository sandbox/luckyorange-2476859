CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
The Lucky Orange module adds a configurable link to the Administrator
toolbar for quick access to a user's Lucky Orange Dashboard. It also adds the
required tracking code to the site's front-end pages with no coding required.


## Don't know what Lucky Orange is?
Lucky Orange will help you answer the question of why 99% of visitors that
visit your site never turn into customers. It's one of those tools that will
have you wondering how you ever lived without it.


### Visitor Recordings
Watch anonymous recordings of visitors navigating and interacting with your
site, just like watching a movie. You will be able to see every mouse movement,
click, and scroll which will help you answer the question of what are my
visitors really doing on my website? Perhaps their never even seeing your call
to action button, or maybe it doesn't even work. Discover a wealth of
information in seconds with our visitor recordings.


### Heat Maps
Aggregate thousands of visitors browsing data into gorgeous heat maps which
show you how people click, move, and scroll all the pages of your site.


### Polls
Ask your customers quick and non-intrusive poll questions, such as "Is there
anything preventing you from purchasing today?" Gain valuable and actionable
information directly from your visitors.


### Form Analytics
Discover the key fields on your forms where customers are abandoning the
checkout or signup process. Learn what fields are taking too long to finish,
which fields are being repeated due to errors, and which fields cause the most
abandonment.


### Support & Sales Chat
Talk directly with your customers with our fully featured chat system. You can
even watch their screen while you chat with them as well as point to areas
directly on their screen for further clarification.


## So Many Features, You Won't Believe It!
 * Fast & Easy Install (No Coding Required)
 * Form Analytics
 * Visitor Recordings
 * Heat Maps
 * Super Easy, Feature-Rich Chat
 * Polls
 * Realtime Analytics
 * Historical Analytics
 * Realtime Twitter Search
 * Funnel Analysis
 * Dedicated Support
 * Free Trial


** Try it for free. You'll love it! **


 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/luckyorange/2476859


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/2476859


 * To learn more about what Lucky Orange has to offer, visit:
   https://www.luckyorange.com


REQUIREMENTS
------------
No special requirements


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
Enabling the Lucky Orange module will add the Lucky Orange tracking
code to each front-end page of the site. A link to the user's Lucky Orange
Dashboard will also be added to the Admin toolbar by default. This can be
changed by editing the config options described below:


 * Explicitly set a site ID to be used in the tracking code snippet by going to
   Administration » Configuration » Web services » Lucky Orange.


 * The Administrator toolbar link can also be configured by going to
   Administration » Configuration » Web services » Lucky Orange.


   - Show Toolbar Link


     When checked, the Lucky Orange link is displayed in the Admin toolbar.
     When unchecked, the link won't be displayed.


   - Show Icon


     When checked, the toolbar link will include the freshness of an orange.
     When unchecked, the link will be displayed in its text-only form.


   - Link Position


     The link can be displayed on the far left side of the Admin toolbar, or it
     can be displayed on the far right side.


MAINTAINERS
-----------
 * Ben Stock - https://drupal.org/u/luckyorange
 